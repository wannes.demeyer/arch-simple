# arch-simple

A simple theme with an Arch Linux wallpaper and a basic Polybar config

## Dependencies

- [picom](https://github.com/yshui/picom)
- [polybar](https://github.com/polybar/polybar)
- [terminus-font](http://terminus-font.sourceforge.net/)
- [dmenu](https://tools.suckless.org/dmenu)
- [feh](https://feh.finalrewind.org/)

## Screenshot

![Desktop](./images/desktop1.png)
![Desktop](./images/tile.png)
![Desktop](./images/app_menu.png)
![Desktop](./images/dolphin.png)

## Installation (using [leftwm-theme](https://github.com/leftwm/leftwm-theme))

You can install this theme using the following commands:

```BASH
leftwm-theme update
leftwm-theme install arch-simple
leftwm-theme apply arch-simple
```

